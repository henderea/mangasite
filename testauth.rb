#!/usr/bin/env ruby

$LOAD_PATH << File.dirname(__FILE__)

#require 'lib/auth'

#resp, data = Auth::request('/')
#Auth::debug = true
#Auth::debug_resp(resp)
#Auth::debug(resp.body)

#require 'lib/list'
#
#Auth.debug = true
#list = List::get_list
#
#puts list.count
                #List::print_list

require 'lib/series'

Signal.trap('SIGINT') {
  puts "\nLoading canceled\n\n"
  begin
    DataManager::save_all
  rescue
    # ignored
  end
  exit 1
}
update_id = DataManager::generate_update_id
#Auth::debug = true
MAX_COUNT = 500
until DataManager::load_all_series(update_id, false, false, false, MAX_COUNT, true)
  #ignore
end
next_list_update, next_series_update = DataManager::next_update
puts "next list update: #{next_list_update.strftime(DataManager::DATE_TIME_FORMAT)}\nnext series update: #{next_series_update.strftime(DataManager::DATE_TIME_FORMAT)}"

#puts MangaSeries::inspect_hash_list(MangaSeries::genre)
#puts MangaSeries::inspect_hash_list(MangaSeries::creator)