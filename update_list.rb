#!/usr/bin/env ruby

$LOAD_PATH << File.dirname(__FILE__)

require 'lib/series'

MAX_COUNT      = 500
PAUSE_DURATION = 60*5
LOG_FILE_NAME = "#{Auth::path}/update.log"
while true
  File::delete(LOG_FILE_NAME) if File::exist?(LOG_FILE_NAME)
  update_id     = DataManager::generate_update_id
  until DataManager::load_all_series(update_id, false, false, false, MAX_COUNT, false, LOG_FILE_NAME)
    #ignore
  end
  next_list_update, next_series_update = DataManager::next_update
  Auth::log(LOG_FILE_NAME, "next list update: #{next_list_update.strftime(DataManager::DATE_TIME_FORMAT)}")
  Auth::log(LOG_FILE_NAME, "next series update: #{next_series_update.strftime(DataManager::DATE_TIME_FORMAT)}")
  sleep(PAUSE_DURATION)
end