#!/usr/bin/env ruby

require 'rubygems'
require 'daemons'

#pwd = Dir.pwd
pwd = '/var/git/MangaSite'
Daemons.run_proc('update_list.rb', { :dir_mode => :normal, :dir => '/opt/pids/manga' }) do
  Dir.chdir(pwd)
  exec 'ruby update_list.rb'
end
