#!/usr/bin/env ruby

require 'rubygems'
require 'daemons'

#pwd = Dir.pwd
pwd = '/var/git/MangaSite'
Daemons.run_proc('server.rb', { :dir_mode => :normal, :dir => '/opt/pids/sinatra' }) do
  Dir.chdir(pwd)
  exec 'ruby server.rb'
end
