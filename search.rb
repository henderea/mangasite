#!/usr/bin/env ruby

$LOAD_PATH << File.dirname(__FILE__)

require 'readline'
require 'lib/auth'
require 'lib/series'
require 'lib/ask'

#Auth::debug = true
while true
  search = Readline.readline('Search for (ENTER to exit): ', true)
  break if search.nil? || search.empty?
  results      = DataManager::search(search)
  result_names = []
  results.each { |v|
    result_names << v.name
  }
  Ask::ask('Which series would you like to look at?', result_names + [:Cancel]) { |opt, ind|
    unless opt == :Cancel
      series = DataManager::get_series(opt)
      puts series.inspect
      chapters = series.chapters
      if chapters.count > 0
        Ask::ask("There are #{chapters.count} chapters. What chapters would you like to download?", [:First, :All, :One, :None]) { |opt, ind|
          if opt == :First
            ChapterDownload::download_imgs(series.url, chapters.first, true)
          elsif opt == :All
            ChapterDownload::download_all(series.url, chapters, true, true)
          elsif opt == :One
            chapter_strings = {}
            chapters.each { |v|
              chapter_strings[v.to_s] = v
            }
            Ask::ask_autocomplete('Which chapter would you like to download?', chapter_strings.keys) { |opt|
              ChapterDownload::download_imgs(series.url, chapter_strings[opt], true) unless opt.nil?
            }
          end
        }
        #if chapters.count > 30
        #  ChapterDownload::download_imgs(series.url, chapters.first, true)
        #else
        #  ChapterDownload::download_all(series.url, chapters, true)
        #end
      else
        puts 'There are no chapters.'
      end
    end
  }
end