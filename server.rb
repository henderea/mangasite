#!/usr/bin/env ruby

$LOAD_PATH << File.dirname(__FILE__)

require 'sinatra'
require 'uri'
require 'lib/auth'
require 'lib/series'
require 'lib/maputil'

set :bind, 'ec2-54-226-88-169.compute-1.amazonaws.com' if RUBY_PLATFORM == 'x86_64-linux'

get '/series/:name/chapter/:chapter/image/:file' do
  name         = params[:name]
  chapter_name = params[:chapter]
  series       = DataManager::get_series(name)
  chapter      = series.chapters.first(:name => chapter_name.to_f)
  if chapter.done
    path = "#{series.url[1..-1]}/#{chapter.to_s}/#{params[:file]}"
    send_file(path, :disposition => 'inline')
  else
    halt 404
  end
end

get '/series/:name/chapter/:chapter' do
  @name         = params[:name]
  @chapter_name = params[:chapter]
  @series       = DataManager::get_series(@name)
  @chapter      = @series.chapters.first(:name => @chapter_name.to_f)
  erb :chapter
end

get '/series/:name' do
  @name   = params[:name]
  @series = DataManager::get_series(@name)
  erb :series
end

get '/genre/:name' do
  @name = params[:name]
  @list = DataManager::get_genre(@name)
  erb :list
end

get '/creator/:name' do
  @name = params[:name]
  @list = DataManager::get_creator(@name)
  erb :list
end

get '/results' do
  @query = params[:query]
  @list  = DataManager::search(@query)
  erb :results
end

get '/search' do
  erb :search
end

get '/check-email' do
  used_emails = %w(henderea@rose-hulman.edu mshenderson@mac.com thairp@rose-hulman.edu)
  email = params[:email]
  used_emails.include?(email) ? '0' : '1'
end

get '/signup' do
  erb :signup
end

get '/' do
  @list = MangaSeries.all(:active => true, :order => [:name.asc])
  erb :home
end