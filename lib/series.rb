$LOAD_PATH << File.dirname(__FILE__)

require 'pstore'
require 'fileutils'
require 'date'
require 'openssl'
require 'auth'
require 'db'

class DataManager
  DATE_TIME_FORMAT = '%A, %B %-d, %Y %-I:%M:%S %P %z'

  def self.generate_update_id
    OpenSSL::Digest::MD5.hexdigest(DateTime::now.strftime('%Q'))
  end

  def self.update_list(force = false, display_progress = false, log_file = nil)
    load_list_timestamp
    if force || @@list_timestamp.nil? || (DateTime::now - @@list_timestamp) > MAX_LIST_AGE
      resp, data = Auth.request(PATH)
      Auth.debug_resp(resp)
      if resp.kind_of?(Net::HTTPSuccess)
        body  = resp.body
        index = REGEX2.match(body).end(0)
        cnt   = 0
        until (md = REGEX.match(body, index)).nil?
          cnt += 1
          print "\rFound #{cnt} series so far" if display_progress
          index = md.end(0)
          url   = md[:url]
          name  = md[:text]
          MangaSeries.first_or_create({ :name => name }, { :url => url })
        end
        puts if display_progress
        @@list_timestamp = DateTime::now
        save_list_timestamp
      else
        Auth::log(log_file, "List get failure: #{resp.code}")
        puts "List get failure: #{resp.code}" if display_progress
      end
    end
  end

  def self.load_all_series(update_id, force = false, force_list = false, recheck_all = false, limit = -1, display_progress = false, log_file = nil)
    done = false
    Auth::run_safe(5, display_progress ? Auth::PRINT_LIMIT : 0, log_file) {
      update_list(force_list, display_progress)
      list2 = MangaSeries.all(:active => false, :last_update.lt => (recheck_all ? DateTime::now : DateTime::now - RECHECK_AGE))
      list2 = list2.all(:update_id.not => update_id) + list2.all(:update_id => nil)
      list2.each { |v|
        v.active      = true
        v.last_update = DateTime::now - (MAX_AGE + 0.1)
        v.save
      }
      age_condition = force ? DateTime::now : (DateTime::now - MAX_AGE)
      list          = MangaSeries.all(:active => true, :last_update.lt => age_condition)
      list          = list.all(:update_id.not => update_id) + list.all(:update_id => nil)
      list_count    = list.count
      if list_count == 0
        Auth::log(log_file, "No entries to update.  All #{MangaSeries.count(:active => true)} are up to date.")
        puts "No entries to update.  All #{MangaSeries.count(:active => true)} are up to date."
        return true
      end
      list.each_with_index { |v, i|
        name = v.name
        url  = v.url
        Auth::log(log_file, "Loading series #{i+1} of #{list_count}")
        print "\rLoading series #{i+1} of #{list_count}" if display_progress
        load_url(name, url, update_id)
        if limit > 0 && (i + 1) >= limit
          Auth::log(log_file, "Reached limit of #{limit} at i = #{i+1} (of #{list_count})")
          puts "\rReached limit of #{limit} at i = #{i+1} (of #{list_count})#{' '*10}" if display_progress
          save_all
          return false
        end
      }
      Auth::log(log_file, "Finished loading #{list_count} series")
      puts "\rFinished loading #{list_count} series#{' ' * (15 + list_count.to_s.length)}" if display_progress
      done = true
    }
    save_all if done
    return done
  end

  def self.next_update
    load_list_timestamp
    oldest_series      = MangaSeries.min(:last_update, { :active => true })
    current_time       = DateTime::now
    #offset_diff = oldest_series.offset - current_time.offset
    oldest_series2     = set_offset(current_time, oldest_series)
    next_list_update   = (@@list_timestamp.nil? || (current_time - @@list_timestamp) > MAX_LIST_AGE) ? DateTime::now : @@list_timestamp + MAX_LIST_AGE
    next_series_update = (current_time - oldest_series2) > MAX_AGE ? DateTime::now : oldest_series2 + MAX_AGE
    return next_list_update, next_series_update
  end

  def self.set_offset(current_time, time_to_change)
    DateTime::new(time_to_change.year, time_to_change.mon, time_to_change.mday, time_to_change.hour, time_to_change.min, time_to_change.sec, current_time.offset)
  end

  def self.save_all
    MangaSeries.all.save
    Genre.all.save
    Creator.all.save
  end

  def self.get_series(name)
    MangaSeries.first(:name => name)
  end

  def self.search(criteria)
    MangaSeries.all({ :active => true, MangaSeries.titles.title.like => "%#{criteria}%" })
  end

  def self.inspect_hash(hash, indent = INDENT)
    if hash.nil?
      'nil'
    else
      str = '{'
      hash.each { |v|
        str << "\n#{' '*indent}#{v[0]} => #{v[1]}"
      }
      str << "\n}"
      str
    end
  end

  def self.inspect_hash_list(hash, indent = INDENT)
    if hash.nil?
      'nil'
    else
      str = '{'
      hash.each { |v|
        str << "\n#{' '*indent}#{v[0]} => #{inspect_list(v[1], indent * 2, indent)}"
      }
      str << "\n}"
      str
    end
  end

  def self.inspect_list(list, indent = INDENT, ending_indent = 0)
    if list.nil?
      'nil'
    else
      str = '{'
      list.each { |v|
        str << "\n#{' '*indent}#{v}"
      }
      str << "\n"
      str << (' ' * ending_indent) if ending_indent > 0
      str << '}'
      str
    end
  end

  def self.make_ranges(list)
    if list.nil?
      'nil'
    else
      ranges    = []
      start_num = nil
      end_num   = nil
      list.each { |i|
        start_num = i if start_num.nil?
        end_num = i if end_num.nil?
        if end_num < i - 1
          ranges << ((start_num == end_num) ? "#{start_num}" : "#{start_num}-#{end_num}")
          start_num = i
        end
        end_num = i
      }
      ranges << ((start_num == end_num) ? "#{start_num}" : "#{start_num}-#{end_num}")
      ranges.join(', ')
    end
  end

  def self.inspect_hash_dm(hash, indent = INDENT)
    if hash.nil?
      'nil'
    else
      str = '{'
      hash.each { |v|
        str << "\n#{' '*indent}#{v.inspect}"
      }
      str << "\n}"
      str
    end
  end

  def self.inspect_list_dm(list, indent = INDENT, ending_indent = 0)
    if list.nil?
      'nil'
    else
      str = '{'
      list.each { |v|
        str << "\n#{' '*indent}#{v.inspect}"
      }
      str << "\n"
      str << (' ' * ending_indent) if ending_indent > 0
      str << '}'
      str
    end
  end

  def self.make_ranges_dm(list)
    if list.nil?
      'nil'
    else
      ranges    = []
      start_num = nil
      end_num   = nil
      list.each { |v|
        i = v.to_f
        start_num = i if start_num.nil?
        end_num = i if end_num.nil?
        if end_num != i - 1 && end_num != i
          ranges << ((start_num == end_num) ? "#{start_num}" : "#{start_num}-#{end_num}")
          start_num = i
        end
        end_num = i
      }
      ranges << ((start_num == end_num) ? "#{start_num}" : "#{start_num}-#{end_num}")
      ranges.join(', ')
    end
  end

  def self.get_genre(genre)
    Genre.first(:name => genre).manga_series
  end

  def self.get_creator(creator)
    Creator.first(:name => creator).manga_series
  end

  private

  #region list constants
  MAX_LIST_AGE      = 0.5
  PATH              = '/manga/list'
  REGEX             = Regexp.new('<div class="c_h2b?">.+?<a href="(?<url>.+?)">(?<text>.+?)</a>\s*</div>', Regexp::MULTILINE)
  REGEX2            = Regexp.new('<div class="c_h1b?">.+?</div>')
  PSTORE_NAME       = 'list_timestamp.pstore'
  #endregion
  #region series constants
  MAX_AGE           = 0.5
  RECHECK_AGE       = 1
  INDENT            = 4
  TITLE_REGEX       = Regexp.new('<td .*?valign="top"><b>Title\(s\):</b></td>', Regexp::MULTILINE)
  CREATOR_REGEX     = Regexp.new('<td .*?valign="top"><b>Creator\(s\):</b></td>', Regexp::MULTILINE)
  GENRE_REGEX       = Regexp.new('<td .*?valign="top"><b>Genres:</b></td>', Regexp::MULTILINE)
  START_DATE_REGEX  = Regexp.new('<td .*?valign="top"><b>Start Date:</b></td>', Regexp::MULTILINE)
  STATUS_REGEX      = Regexp.new('<td .*?valign="top"><b>Status:</b></td>', Regexp::MULTILINE)
  SUMMARY_REGEX     = Regexp.new('<td .*?valign="top"><b>Summary:</b></td>', Regexp::MULTILINE)
  STATUS_CELL_REGEX = Regexp.new('<td.*?>\s*<span.*?>\s*(?<contents>.*?)\s*</span>\s*</td>', Regexp::MULTILINE)
  CELL_REGEX        = Regexp.new('<td.*?>\s*(?<contents>.*?)\s*</td>', Regexp::MULTILINE)
  LINK_REGEX        = Regexp.new('<a href="(?<url>.+?)">\s*(?<text>.+?)\s*</a>', Regexp::MULTILINE)
  CHAPTERS_REGEX    = Regexp.new('<div class="c_h1.?" style="margin-top:1px;">Chapters</div>')
  CHAPTER_REGEX     = Regexp.new('<div.*?>\s*<div.*?>\s*<a .*?href=".+?">.*?<strong>\s*(?<num>\d+\.?\d*)\s*</strong>\s*</a>.*?</div>\s*<div class="clear">\s*</div>\s*</div>')
  #endregion

  def self.setup
    @list_timestamp = nil
  end

  def self.load_list_timestamp
    ds               = DataStore.get_instance
    current_time     = DateTime::now
    @@list_timestamp = set_offset(current_time, ds.list_timestamp)
  end

  def self.save_list_timestamp
    ds                = DataStore.get_instance
    ds.list_timestamp = @@list_timestamp
    ds.save
  end

  def self.load_url(name, url, update_id)
    last_update       = DateTime::now
    #puts "name: #{name}; url: #{url}"
    resp, data        = Auth::request(url)
    entry             = MangaSeries.first_or_new({ :name => name }, { :url => url })
    entry.last_update = last_update
    entry.update_id   = update_id
    if resp.kind_of?(Net::HTTPSuccess)
      body = resp.body
      begin
        index = TITLE_REGEX.match(body).end(0)
      rescue
        entry.active = false
        entry.save
        return
      end
      title_text, index   = read_cell(body, index)
      titles              = split_list(title_text)
      index               = CREATOR_REGEX.match(body, index).end(0)
      creator_text, index = read_cell(body, index)
      creators            = parse_links(creator_text)
      index               = GENRE_REGEX.match(body, index).end(0)
      genre_text, index   = read_cell(body, index)
      genres              = parse_links(genre_text)
      index               = START_DATE_REGEX.match(body, index).end(0)
      start_date, index   = read_cell(body, index)
      index               = STATUS_REGEX.match(body, index).end(0)
      status, index       = read_cell(body, index, STATUS_CELL_REGEX)
      index               = SUMMARY_REGEX.match(body, index).end(0)
      summary, index      = read_cell(body, index)
      chapters            = get_chapters(body, index)
      entry.active        = true
      resolve_titles(entry, titles)
      resolve_creators(entry, creators)
      resolve_genres(entry, genres)
      entry.start_date = start_date
      entry.status     = status
      entry.summary    = summary
      resolve_chapters(entry, chapters)
    else
      entry.active = false
    end
    entry.save
  end

  def self.resolve_titles(entry, titles)
    entry.titles.inspect
    entry.titles.destroy unless entry.titles.empty?
    titles.each { |v|
      entry.titles << Title.create(:title => v)
    }
  end

  def self.resolve_creators(entry, creators)
    creators_list = []
    creators.each { |v|
      creators_list << Creator.first_or_create({ :name => v[0] }, { :url => v[1] })
    }
    entry.creators = creators_list
  end

  def self.resolve_genres(entry, genres)
    genres_list = []
    genres.each { |v|
      genres_list << Genre.first_or_create({ :name => v[0] }, { :url => v[1] })
    }
    entry.genres = genres_list
  end

  def self.resolve_chapters(entry, chapters)
    #puts
    #puts Chapter.all.inspect
    #puts entry.inspect
    #puts chapters.inspect
    #puts entry.chapters.inspect
    chapters.each { |v|
      entry.chapters << Chapter.first_or_create({ :manga_series => entry, :name => v })
    }
  end

  def self.read_cell(body, index, regex = CELL_REGEX)
    md       = regex.match(body, index)
    contents = md[:contents]
    return contents, md.end(0)
  end

  def self.split_list(text)
    text.split(%r{\s*<br\s*/>\s*})
  end

  def self.parse_links(text)
    index = 0
    list  = {}
    until (md = LINK_REGEX.match(text, index)).nil?
      index      = md.end(0)
      url        = md[:url]
      name       = md[:text]
      list[name] = url
    end
    list
  end

  def self.get_chapters(body, index)
    list  = []
    #puts index
    index = CHAPTERS_REGEX.match(body, index).end(0)
    #puts index
    until (md = CHAPTER_REGEX.match(body, index)).nil?
      index = md.end(0)
      #puts index
      num   = md[:num].to_f
      list << num
    end
    list.sort
  end

  self.setup
end

class ChapterDownload
  def self.download_all(url, chapters, display_progress = false, one_line = false)
    start_time = Time::now
    chapters.each_with_index { |chapter, i|
      download_imgs(url, chapter, display_progress, "(Chapter #{i+1} (#{chapter.to_s}) of #{chapters.count})", one_line)
    }
    end_time = Time::now
    diff     = end_time - start_time
    h, m, s  = get_time_parts(diff)
    puts "\rDownloaded #{chapters.count} chapters in #{h} hours, #{m} minutes, and #{s} seconds#{' ' * (chapters.count.to_s.length*2)}" if display_progress
  end

  def self.get_time_parts(time)
    return (time / 3600.0).floor, ((time % 3600) / 60.0).floor, '%06.3f' % (time % 60)
  end

  def self.output(str, one_line = false)
    if one_line
      print str
    else
      puts str
    end
  end

  def self.download_imgs(url, chapter, display_progress = false, append_str = nil, one_line = false)
    append_str = "(Chapter #{chapter.to_s})" if append_str.nil?
    chapter_url = make_chapter_url(url, chapter.to_s)
    done        = chapter.done
    if done
      output("\rAlready downloaded chapter #{append_str}#{' '*append_str.length}", one_line) if display_progress
    else
      #puts chapter_url
      resp = nil?
      Auth::run_safe { resp, data = Auth::request(chapter_url) }
      if !resp.nil? && resp.kind_of?(Net::HTTPSuccess)
        body  = resp.body
        #puts body
        index = 0
        paths = []
        until (md = IMAGE_REGEX.match(body, index)).nil?
          index = md.end(0)
          paths << md[:path]
        end
        finished = true
        paths.each_with_index { |path, i|
          print "\rDownloading file #{i+1} of #{paths.count} #{append_str}#{' '*append_str.length}" if display_progress
          finished = false unless download_file(url, chapter, path)
        }
        output("\rFinished Downloading #{paths.count} files #{append_str}#{' '*(paths.count.to_s.length * 2)}", one_line) if display_progress
        done = finished
      end
    end
  ensure
    chapter.done = done
    chapter.save
  end

  private

  START_REGEX = %r{<a class="readscroll".*?>.*?</a>}
  IMAGE_REGEX = %r{<img class="dyn" src="http://img.starkana.com(?<path>.+?)".*?/>}

  def self.setup
    @@net = Net::HTTP.new('manga.starkana.com')
  end

  def self.download_file(url, chapter, file_url)
    path      = make_directory(url, chapter)
    file_name = File.basename(file_url)
    fname     = "#{path}/#{file_name}"
    finished  = false
    if File.exist?(fname)
      finished = true
    else
      Auth::run_safe {
        f = File.open(fname, 'wb')
        begin
          @@net.request_get(file_url) { |resp|
            resp.read_body { |segment|
              f.write(segment)
            }
          }
          finished = true
        ensure
          f.close
          File.delete(fname) unless finished
        end
      }
    end
    finished
  end

  def self.make_directory(url, chapter)
    path = "#{url[1..-1]}/#{chapter}"
    unless Dir.exist?(path)
      parts = path.split('/')
      paths = []
      parts.each { |i|
        paths << (paths.empty? ? i : "#{paths.last}/#{i}")
      }
      paths.each { |i|
        unless Dir.exist?(i)
          Dir.mkdir(i)
        end
      }
    end
    path
  end

  def self.make_chapter_url(url, chapter)
    "#{url}/chapter/#{chapter}?scroll"
  end

  self.setup
end