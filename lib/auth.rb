$LOAD_PATH << File.dirname(__FILE__)

require 'net/http'
require 'pstore'
require 'date'
require 'db'

class Auth
  PRINT_LIMIT = 1

  def self.debug?
    @@debug
  end

  def self.debug=(debug)
    @@debug = debug
  end

  def self.debug(output)
    puts output if @@debug
  end

  def self.debug_resp(resp)
    debug("Code: #{resp.code}")
    debug("Message: #{resp.message}")
    if @@debug
      resp.each { |key, val| puts key + ' = ' + val }
    end
  end

  def self.request(path, headers = {})
    load_cookie
    do_auth
    resp, data = do_request(path, headers)
    save_cookie
    return resp, data
  end

  def self.path
    linux? ? '/var/git/MangaSite' : FileUtils::pwd
  end

  def self.linux?
    RUBY_PLATFORM == 'x86_64-linux'
  end

  def self.log(log_file, message)
    unless log_file.nil?
      f = File.new(log_file, 'a')
      begin
        f << "[#{DateTime::now.strftime('%Y-%m-%d %H:%M:%S.%L %z')}] #{message}\n"
      ensure
        f.close
      end
    end
  end

  def self.run_safe(max_tries = 5, print_limit = PRINT_LIMIT, log_file = nil)
    count = 0
    while max_tries.nil? || count < max_tries
      begin
        yield
      rescue SystemExit, Interrupt, SignalException => e
        raise e
      rescue Exception => e
        log(log_file, e.message)
        e.backtrace.each { |v|
          log(log_file, v.inspect)
        }
        if print_limit.nil? || count < print_limit
          puts e.message
          e.backtrace.each { |v|
            puts v.inspect
          }
        end
      end
      count += 1
    end
  end

  private

  MAX_COOKIE_AGE = 60*60*24*7
  PATH           = '/login'

  def self.setup
    @@http   = Net::HTTP.new('starkana.com')
    @@debug  = false
    @@cookie = nil
  end

  def self.get_cookie(resp)
    resp.response['set-cookie'].split('; ')[0]
  end

  def self.load_cookie
    ds       = DataStore.get_instance
    @@cookie = (ds.cookie.nil? || ds.cookie.empty?) ? nil : ds.cookie
  end

  def self.save_cookie
    ds = DataStore.get_instance
    unless ds.cookie == @@cookie
      ds.cookie             = @@cookie
      ds.last_cookie_update = DateTime::now
      ds.save
    end
  end

  def self.do_request(path, headers = {})
    my_headers = headers
    my_headers['Cookie'] = @@cookie unless @@cookie.nil?

    resp, data = @@http.get(path, my_headers)
    @@cookie = get_cookie(resp) unless resp.response['set-cookie'].nil?
    save_cookie
    return resp, data
  end

  def self.do_auth
    resp, data = do_request(PATH)

    debug_resp(resp)

    unless resp.code == '302'
      data    = 'username=henderea&password=ericallen&remember=on&login=Log+In&n='
      headers = {
          'Cookie'       => @@cookie,
          'Referrer'     => 'http://starkana.com/login',
          'Content-Type' => 'application/x-www-form-urlencoded'
      }

      resp, data = @@http.post(PATH, data, headers)

      debug_resp(resp)

      @@cookie = get_cookie(resp) unless resp.response['set-cookie'].nil?
      save_cookie
    end

    #debug(resp.body)
  end

  self.setup
end