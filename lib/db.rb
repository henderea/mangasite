$LOAD_PATH << File.dirname(__FILE__)

require 'date'
require 'rubygems'
require 'data_mapper'

#DataMapper::Logger.new($stdout, :debug)
#TODO: maybe replace with an explicit path?
#PATH = FileUtils::pwd
#PATH = '/var/git/MangaSite' if RUBY_PLATFORM == 'x86_64-linux'
#DataMapper.setup(:default, "sqlite://#{PATH}/data.db")
DB_USER     = 'manga'
DB_PASS     = 'seMqB5S4VbB2hSaC'
DB_HOSTNAME = RUBY_PLATFORM == 'x86_64-linux' ? 'mangadb.cq56vtkymgj8.us-east-1.rds.amazonaws.com:3306' : '127.0.0.1:3306'
DB_NAME     = 'manga'
DataMapper.setup(:default, "mysql://#{DB_USER}:#{DB_PASS}@#{DB_HOSTNAME}/#{DB_NAME}")
DataMapper::Property::String.length(255)

#region Data Model
#region Series
class MangaSeries
  include DataMapper::Resource

  property :name, String, :key => true
  has n, :titles, :order => [:title.asc]
  has n, :creators, :through => Resource, :order => [:name.asc]
  has n, :genres, :through => Resource, :order => [:name.asc]
  property :start_date, String
  property :status, String
  property :summary, Text
  has n, :chapters, :order => [:name.asc]
  property :last_update, DateTime, :default => (DateTime::now - 1)
  property :url, String
  property :active, Boolean, :default => true
  property :update_id, String

  def inspect
    "name => #{self.name}\nTitles => #{DataManager::inspect_list_dm(self.titles)}\nCreators => #{DataManager::inspect_hash_dm(self.creators)}\nGenres => #{DataManager::inspect_hash_dm(self.genres)}\nStart Date => #{self.start_date}\nStatus => #{self.status}\nSummary => #{self.summary}\nChapters => #{DataManager::make_ranges_dm(self.chapters)}\nLast Update => #{self.last_update.strftime(DataManager::DATE_TIME_FORMAT)}\nURL => #{self.url}"
  end
end
#endregion

#region Title
class Title
  include DataMapper::Resource

  property :id, Serial
  property :title, String

  belongs_to :manga_series

  def inspect
    self.title
  end
end
#endregion

#region Creator
class Creator
  include DataMapper::Resource

  property :name, String, :key => true
  property :url, String

  has n, :manga_series, :through => Resource, :order => [:name.asc]

  def inspect
    "#{self.name} => #{self.url}"
  end
end
#endregion

#region Genre
class Genre
  include DataMapper::Resource

  property :name, String, :key => true
  property :url, String

  has n, :manga_series, :through => Resource, :order => [:name.asc]

  def inspect
    "#{self.name} => #{self.url}"
  end
end
#endregion

#region Chapter
class Chapter
  include DataMapper::Resource

  property :id, Serial
  property :name, Float
  property :done, Boolean

  belongs_to :manga_series

  def to_f
    self.name.to_i == self.name ? self.name.to_i : self.name
  end

  def to_s
    "#{self.name.to_i == self.name ? self.name.to_i : self.name}"
  end
end
#endregion

#region DataStore
class DataStore
  include DataMapper::Resource

  property :id, Serial
  property :cookie, String
  property :last_cookie_update, DateTime, :default => (DateTime::now - 7.5)
  property :list_timestamp, DateTime, :default => (DateTime::now - 1)

  def self.get_instance
    first_or_create
  end
end
#endregion

#region User
class User
  include DataMapper::Resource

  property :username, String, :key => true
  property :first_name, String
  property :last_name, String
  property :email_address, String
  property :password, String
  property :birthday, Date
  #continue
end
#endregion
#endregion

DataMapper::finalize
DataMapper::auto_upgrade!